
//emits message based on degree of longitude.

map = function()
  {
    var longLine = Math.floor(this.location.longitude);
    emit({
      longitude: longLine
    }, {
        count: 1
    });
  }


// reduce function, receive and count

  reduce = function (key,values)
  {
    var total = 0;
    for (var i = 0; i < values.length; i++)
    {
      total += values[i].count;
    }
    return {
      count: total
    };
  }



  // cursor to iterate over the results.

  results = db.runCommand({
    mapReduce: 'cities',
    map: map,
    reduce: reduce,
    out: 'cities.report'
  })
