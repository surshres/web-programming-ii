import angular from 'angular';
const ngRoute = require('angular-route');
import routing from './newComponent.routes';

export class newComponentController {
  /*@ngInject*/
   constructor($routeParams, User)
   {
     this.User = User;
     this.getUserDataById($routeParams.id);
   }
   $onInit()
   {

   }

   getUserDataById(id)
   {
     this.User.getUserById(id)
       .then(response => {
         this.userById = response.data;
       })
       .catch(error => {
         console.error(error);
       });
   }
}


 export default angular.module('comp3705App.newComponent', [ngRoute])
  .config(routing)
  .component('newComponent', {
    template: require('./newComponent.html'),
    controller: newComponentController,
    controllerAs: 'newComponentController'
  })
  .name;
