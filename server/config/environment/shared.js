'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  about: {
    name: {
      first: 'Suraaj',
      last: 'Shrestha'
    },
    email: 'suraaj.shrestha@du.edu',
    course: 'COMP-3705'
  }
};
