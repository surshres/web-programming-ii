/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';
import * as recipe from './api/recipe';

export default function(app) {
  // All routes under /api/users should be handled by the
  // exported router inside ./api/users/index.js
  app.use('api/users', recipe.RouterOfUser);
  app.use('api/recipe', recipe.RouterOfRecipe);

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
